# -*- coding: utf-8 -*-

def circle_blue():
    """Fizz-buzz variant.
    Prints number 1 to 100 UNLESS:
    - num divisible by 5 -> print "Circle" 
    - num divisible by 7 -> print "Blue" 
    - num divisible by 5 & 7 -> print "Circle Blue" 
    """
    for n in range(1, 101):
        if n % 5 == 0 and n % 7 == 0:
            print("Circle Blue")
        elif n % 5 == 0:
            print("Circle")
        elif n % 7 == 0:
            print("Blue")
        else:
            print(n)


if __name__ == "__main__":
    circle_blue()

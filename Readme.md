## Assignments repository - CircleBlue

## Setup

(requires python3.6 or newer)


-  `python3 -m venv .venv`

	- will create a new virtual environment

-  `source .venv/bin/activate`

	- will activate the new venv

-  `pip install -r requirements.txt`

	- will install dependencies (asab and indirect deps)


## Running the scripts
- Task 1.
    - `python3 circle_blue.py`
        - conditionally prints numbers 1 to 100

- Task 2.
    - `python3 cc_webapp.py`
        - JSON REST apion localhost:9000
        - supports `/test`, `/exchange`

### Example 1: `$ curl http://localhost:9000/test`
```
{"succes": true}
```

### Example 2: `$ curl http://localhost:9000/exchange`

```
{   
    "success": true, 
    "data": {
        "rates": {
            "CAD": 1.5631,
            "HKD": 9.0986,
            "ISK": 162.0,
            ...
        },
        "base": "EUR",
        "date": "2020-09-22"
    }
}
```

Example 2 error state response:
```
{"succes": false}
```
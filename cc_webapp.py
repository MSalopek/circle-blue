
#!/usr/bin/env python3
import asab
import aiohttp
import json
import logging

import asab.web
import asab.web.rest

_L = logging.getLogger(__name__)

class AsabApp(asab.Application):
    """Running: python3 cc_webapp.py"""

    async def initialize(self):
        # Loading the web service
        self.add_module(asab.web.Module)
        websvc = self.get_service("asab.WebService")
        container = asab.web.WebContainer(websvc, 'simple:webserver', config={"listen": "0.0.0.0 9000"})

        # Add a routes
        container.WebApp.router.add_get('/test', self.test)
        container.WebApp.router.add_get('/exchange', self.get_exchange_latest)

    async def get_exchange_latest(self, request):
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get('https://api.exchangeratesapi.io/latest', timeout=5) as resp:
                    data = {"success": True}
                    data["data"] = await resp.json()
                    if resp.status != 200:
                        data = {"success": False}
                        # uninformative from REST api standpoint but can happen
                        _L.error("failed https://api.exchangeratesapi.io/latest GET - status: {}".format(resp.status))
                        return asab.web.rest.json_response(request=request, data=data)

                    _L.info("succes GET - https://api.exchangeratesapi.io/latest")
                    return asab.web.rest.json_response(request=request, data=data)
        except Exception as err:
            _L.error("failed https://api.exchangeratesapi.io/latest GET - error: {}".format(err))
            data = {"success": False}
            return asab.web.rest.json_response(request=request, data=data)
        
    async def test(self, request):
        return asab.web.rest.json_response(request=request, data={"success": True})


if __name__ == '__main__':
    app=AsabApp()
    app.run()
